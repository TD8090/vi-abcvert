var log = function (r) {console.log(r)};
var processXML = function(){
    var fromNinja, venues = [], periods = [], stations = [], menuitems = []
        , _venuehtml = [], _periodhtml = [], _stationhtml = []
        , venuenames = [], periodnames = [], stationnames = [], menuitemnames = [], i, j, k, l
        , br = '<br/>'
        , h1_open = '<strong style="font-size:22px; text-decoration: underline;">'
        , h1_close = '</strong>'
        , h2_open = '<span style="font-size:20px;">'
        , h2_close = '</span>'
        , icon_home = '   <div class="icon_home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></div> '
        , icon_time = '<span class="glyphicon glyphicon-time" style="margin-left: 31px;" aria-hidden="true"></span> '
        , icon_disabled = '<button class="btn btn-default btn-xs" disabled="disabled"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></button> '
        , icon_cutlery = '<span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span>'
        , stationspan_open = '<span class="f-stations">'
        , stationspan_close = '</span>'
        , itemspan_open = '<span class="menuitems">'
        , itemspan_close = '</span>'
        , expand_href_open1 = '<a class="btn btn-default btn-xs" role="button" data-toggle="collapse" href="#'
        , expand_href_open2 = '" aria-expanded="false" aria-controls="'
        , expand_button_open1 = '<button class="btn btn-default btn-xs" role="button" data-toggle="collapse" data-target="#'
        , expand_button_open2 = '" aria-expanded="false" aria-controls="'
        , expand_button_open3 = '">'
        , expand_href_close = '</a>'
        , expand_button_close = '</button> '
        , itemz_expand_open1 = '<div class="collapse" id="'
        , itemz_expand_open2 = '"><div class="itemz panel panel-info">'
        , expand_close = '</div></div>'
        , checkbox = '<button type="button" class="btn btn-xs btn-default btncheck" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>'
        , tog_comments = '<button type="button" id="btntogcomment" onClick="showComments(this)" class="btn btn-xs btn-default" data-toggle="button" aria-pressed="false" autocomplete="off"><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></button>'
        , stationcomments = '<input size="3" class="stationcomments" style="display:none;" type="text" onchange="sizer(this);" onkeyup="this.onchange();" onpaste="this.onchange();" oninput="this.onchange();" />'
        , Tree = $('#Tree')
        , rgx_venues = /(&lt;Venue [\s\S]*?&lt;\/Venue&gt;)/g
        , rgx_venuenames = /&lt;Venue name="([\w\W]*?)"\/?&gt;/
        , rgx_periods = /(&lt;MealPeriod[\s\S]*?&lt;\/MealPeriod\/?&gt;)/g
        , rgx_periodnames = /&lt;MealPeriod name="([\w\W]*?)"\/?&gt;/
        , rgx_stations = /(&lt;MealStation name="(?:[^\r\n]|\r(?!\n))*?"&gt;[\S\s]*?&lt;\/MealStation&gt;|&lt;MealStation name="(?:[^\r\n]|\r(?!\n))*?"\/&gt;)/g
        , rgx_stations_nochildren = /&lt;MealStation name="(?:[^\r\n]|\r(?!\n))*?"\/&gt;/
        , rgx_stationnames = /&lt;MealStation name="([\w\W]*?)"\/?&gt;/
        , rgx_menuitems = /(&lt;MenuItem[\s\S]*?&lt;\/MenuItem&gt;)/g
        , rgx_menuitemnames = /&lt;MenuItem name="([\w\W]*?)" desc/
        ;
    fromNinja = document.getElementById('NinjaOutput').innerHTML;
    //document.getElementById('JsOutput').innerHTML = fromNinja;

    var venue_match = fromNinja.match( rgx_venues );
    for(i=0;i<venue_match.length;i++){
        periods = venue_match[i].match( rgx_periods );
        venuenames[i] = venue_match[i].match( rgx_venuenames );
        _venuehtml[i] = '';
        _venuehtml[i] = icon_home + h1_open + venuenames[i][1]+ h1_close + tog_comments + br;
        Tree.append(_venuehtml[i]);

        //periods[] now has 5 mealperiod enclosures
        for(j=0;j<periods.length;j++){
            //current mealperiod name to cur_periodname[i]
            periodnames[j] = periods[j].match( rgx_periodnames );
            //append current periodname to output
            _periodhtml[j] = '';
            _periodhtml[j] += icon_time + h2_open + periodnames[j][1]+ h2_close + br;

            Tree.append(_periodhtml[j]);

            //for this mealperiod, store mealstation enclosures
            stations = periods[j].match( rgx_stations );
            for(k=0;k<stations.length;k++){
                //current mealstation name to cur_stationname[j]
                stationnames[k] = stations[k].match( rgx_stationnames );
                //BUTTON before station name to open stations menu items

                /*If stations[k] has no children, display station with ban-circle icon and exit loop*/
                if( rgx_stations_nochildren.test(stations[k]) ) {
                    _stationhtml[k] = '';
                    _stationhtml[k] += icon_disabled + stationspan_open + stationnames[k][1] + stationspan_close + br;
                }else{
                    _stationhtml[k] = '';
                    _stationhtml[k] += checkbox;
                    _stationhtml[k] += expand_button_open1 + stationnames[k][1].replace(/[^a-zA-Z0-9]+/g, '')+i+j+k;
                    _stationhtml[k] += expand_button_open2 + stationnames[k][1].replace(/[^a-zA-Z0-9]+/g, '')+i+j+k;
                    _stationhtml[k] += expand_button_open3;
                    _stationhtml[k] += icon_cutlery + expand_button_close;

                    _stationhtml[k] += stationspan_open + stationnames[k][1] + stationspan_close +stationcomments+br;

                    //for this mealstation, store menuitem enclosures
                    menuitems = stations[k].match(rgx_menuitems);

                    _stationhtml[k] += itemz_expand_open1;
                    _stationhtml[k] += stationnames[k][1].replace(/[^a-zA-Z0-9]+/g, '')+i+j+k;
                    _stationhtml[k] += itemz_expand_open2;
                    for (l = 0; l < menuitems.length; l++) {
                        //for this menu item, name to menuitemnames[l]
                        menuitemnames[l] = menuitems[l].match(rgx_menuitemnames);
                        //add to string all the menuitem names in this(\\k) station
                        _stationhtml[k] += itemspan_open + menuitemnames[l][1] + itemspan_close + br;
                        //for this mealstation, store menuitem enclosures
                    }
                    _stationhtml[k] += expand_close;
                }
                Tree.append(_stationhtml[k]);
            }
        }
    }
};//END: processXML()
var checkinputarea = function() {
    var empty = false;
    if ($('#inputarea').val() == '') {
        empty = true;
    }
    if (empty) {
        $('#readXML').attr('disabled', 'disabled');
    } else {
        $('#readXML').removeAttr('disabled');
    }
};
var addURL = function(){
    var rgx_brand = /http:\/\/([\s\S]*)\.compass-usa\.com\/[\s\S]*.aspx/i;
    var rgx_org = /http:\/\/[\s\S]*\.compass-usa\.com\/([\s\S]*)\/Pages/i;
    var rgx_loc = /http:\/\/[\s\S]*\.compass-usa\.com\/[\s\S]*\/Pages\/SignageXML\.aspx\?location=([\s\S]*)/i;
    var exp_url = $('#successlink').attr('href');
    var thecomments = '';
    var match_brand = exp_url.match(rgx_brand);
    var match_org = exp_url.match(rgx_org);
    var match_loc = exp_url.match(rgx_loc);
    var exp_loc = decodeURIComponent(match_loc[1].toString());
    var exp_brand = match_brand[1].toString();
    var exp_org = match_org[1].toString();
    var venuecomments = [];
    var $this, obj, text, $input;
    $(".f-stations").each(function(){
        $this = $(this);
        $input = $this.next(".stationcomments");
        if ($input.val().length > 0 ){
            text = $this.text();
            log(text);
            obj = {};
            obj[text] = $input.val();
            log(obj);
            venuecomments.push(obj);
            log("f-station done!")
        }
    });
//obj{}
    //log(exp_brand);log(exp_org);log(exp_loc);
    //http://stackoverflow.com/questions/19970301/convert-javascript-object-or-array-to-json-for-ajax-data
    var jsonObject = {};
    jsonObject[exp_loc] =
    {
        "brand":exp_brand,
        "org":exp_org,
        "url":exp_url,
        "comments": venuecomments
    };
    log(jsonObject);
    log(JSON.stringify(jsonObject));
log("poop");
    //jsonObject.metros[index] = JSON.stringify(graph.getVertex(index).getData());
    $.ajax({
        type: "POST", url: "xmlparser_urls.php",
        data: {//http://stackoverflow.com/questions/3921520/writing-json-object-to-json-file-on-server
            json : JSON.stringify(jsonObject)
        },
        dataType : 'json',//json expected in response, comment out all debugging in php.
        success: function(res){
            $('#locsave_response').html(res).show().css('fontsize', '12px').css('font-family','courier');
            //log(res);log("that was the res");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            log("ajax unsuccessful" +"+"+ jqXHR +"+"+ textStatus +"+"+ errorThrown);
            log("and the res text...."+ jqXHR.responseText);
        }
    });
};
var sizer = function(e){
    var size = parseInt($(e).attr('size'));
    var chars = $(e).val().length;
    $(e).attr('size', chars+1);

};
var showComments = function(e){
    if( $( e ).hasClass( "active" ) ) {
        $('.stationcomments').css({'display':'none'});
    }else{
        $('.stationcomments').css({'display':'inline-block'});
    }
};

$(document).ready(function (){
    checkinputarea();
    $('#readXML').attr('disabled', 'disabled');
    $('#inputarea').on('input', checkinputarea());
    //location dropdown -> fill input
    $(".locselect").click(function() {
        var txt = $(this).val();
        $('#inputarea').val('').val(function(i,val){
            return val + txt;
        });
        checkinputarea();
    });
    //comment toggle
    $('#locsave').click(addURL);
/*    $(function(){
        $('#menu_search').keyup(function(){
            var size = parseInt($(this).attr('size'));
            var chars = $(this).val().length;
            if(chars >= size) $(this).attr('size', chars);
        });
    });*/

    /**
     *
     *
     * */

     //run processXML() when #preXML is detected as loaded
    $('#preXML').load(processXML());
});//END: $(document).ready(function (){

